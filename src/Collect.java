import java.util.*;


public class Collect {

    public static List<Integer> generList(int num, int range) { //num кол-во нужных итераций, генерим от 0 до range
        List<Integer> listNum = new ArrayList<>();
        for(int i = 0; i < num; i++) {
            listNum.add((int) (Math.random() * range));
        }
        Collections.sort(listNum);
        return listNum;
    }

    public static List<String> getWordFroString(String str) {
        List<String> strSplit = new ArrayList<>();
        //String temp = str.toLowerCase();  // можно привести к одному регистру - в зд не было...
        for (String s : str.split(" ")) {  // делим строку на слова и
            strSplit.add(s);                    // кладем в коекцию
        }
        List<String> listWord = new ArrayList<String>(new HashSet<String>(strSplit )); //HashSet  хранит только уникальные
        Collections.sort(listWord);                                                  // значения и позже сртируем
        // listWord = Stream.of(temp.split(" ")).distinct() - ой
        //        .collect(Collectors.toList());
        return listWord;
    }


    public static Map<String, Integer> getCharScore(String str) {
        List<String> charSplit = new ArrayList<>();  // повторяю метод со словами, симво тже мжет быть строкой
        for (String s : str.split("")) {
            charSplit.add(s);
        }
        List<String> hashListChar = new ArrayList<String>(new HashSet<String>(charSplit )); // получаю все уникальные символы
        TreeMap<String, Integer> hashMap = new TreeMap<>(); //говрят он быстрее и сортирует по взрастанию ключа
        for (String s1 : hashListChar) {
            int num=0;
            for(int i = 0; i < charSplit.size(); i++) {
                if(s1.equals(charSplit.get(i))){
                    num++;
                }
            }
            hashMap.put(s1,num); // пока не дошло как по ыстрому toString у Map переписать
        }

        return hashMap;
    }

}
